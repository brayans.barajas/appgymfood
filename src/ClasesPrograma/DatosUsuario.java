package ClasesPrograma;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.Buffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class DatosUsuario {

    private String usuario;
    private String nombre;
    private String contraseña;
    private String confirmarContraseña;
    private String dia;
    private String mes;
    private String año;
    private String genero;

    public DatosUsuario() {
    }

    public DatosUsuario(String usuario, String nombre, String contraseña, String confirmarContraseña, String dia, String mes, String año, String genero) {
        this.usuario = usuario;
        this.nombre = nombre;
        this.contraseña = contraseña;
        this.confirmarContraseña = confirmarContraseña;
        this.dia = dia;
        this.mes = mes;
        this.año = año;
        this.genero = genero;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getConfirmarContraseña() {
        return confirmarContraseña;
    }

    public void setConfirmarContraseña(String confirmarContraseña) {
        this.confirmarContraseña = confirmarContraseña;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getAño() {
        return año;
    }

    public void setAño(String año) {
        this.año = año;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    
    
    //se Guarda en un vector las datos del usuario
    public static void guardarvector(DatosUsuario listDatoUsuario) {
        Vector listUsuario = new Vector();
        listUsuario.addElement(listDatoUsuario);
    }

    //crear archivo txt y guardar datos
    public static void guardarArchivo(DatosUsuario listDatoUsuario) {
        try {

            FileWriter archivo = new FileWriter("src\\ClasesPrograma\\BaseDatosUsuario.txt", true);
            BufferedWriter escribirDatos = new BufferedWriter(archivo);
            PrintWriter guardardatos = new PrintWriter(escribirDatos);
            guardardatos.print("Nombre: " + listDatoUsuario.getNombre() + "|");
            guardardatos.print("Usuario: " + listDatoUsuario.getUsuario() + "|");
            guardardatos.print("Contraseña: " + listDatoUsuario.getContraseña() + "|");
            guardardatos.print("ConfirmarContrseña: " + listDatoUsuario.getConfirmarContraseña() + "|");
            guardardatos.print("Dia: " + listDatoUsuario.getDia() + "|");
            guardardatos.print("Mes: " + listDatoUsuario.getMes() + "|");
            guardardatos.print("Año: " + listDatoUsuario.getAño() + "|");
            guardardatos.print("genero: " + listDatoUsuario.getGenero()+ "\n");

            guardardatos.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public static DatosUsuario procesarLineaArchivoPlano(String linea) {
        DatosUsuario usuarioN = new DatosUsuario();

        String[] lineaUsuario = linea.split("\\|");

        String[] lineaTexto = lineaUsuario[0].split("Nombre: ");
        usuarioN.nombre = lineaTexto[1];

        lineaTexto = lineaUsuario[1].split("Usuario: ");
        usuarioN.usuario = lineaTexto[1];

        lineaTexto = lineaUsuario[2].split("Contraseña: ");
        usuarioN.contraseña = String.valueOf(lineaTexto[1]);

        lineaTexto = lineaUsuario[3].split("ConfirmarContrseña: ");
        usuarioN.confirmarContraseña = String.valueOf(lineaTexto[1]);

        lineaTexto = lineaUsuario[4].split("Dia: ");
        usuarioN.dia = lineaTexto[1];

        lineaTexto = lineaUsuario[5].split("Mes: ");
        usuarioN.mes = lineaTexto[1];

        lineaTexto = lineaUsuario[6].split("Año: ");
        usuarioN.año = lineaTexto[1];
        
        lineaTexto = lineaUsuario[7].split("genero: ");
        usuarioN.año = lineaTexto[1];

        return usuarioN;
    }

    public static List<DatosUsuario> leerArchivoUsuario() {
        Stream<String> lines = null;
        Path pathArchvioPlano = Paths.get("src\\ClasesPrograma\\BaseDatosUsuario.txt");
        ArrayList<DatosUsuario> listaUsuarios = new ArrayList<>();
        try {
            lines = Files.lines(pathArchvioPlano);
            List<String> datos = lines.collect(Collectors.toList());
            System.out.println(ZonedDateTime.now().format(DateTimeFormatter.RFC_1123_DATE_TIME));
            for (String dato : datos) {
                DatosUsuario NuevoUsuario = DatosUsuario.procesarLineaArchivoPlano(dato);
                listaUsuarios.add(NuevoUsuario);
            }
        } catch (IOException ioex) {
            System.err.println("Error de IO al leer el archivo plano " + ioex.getMessage());
        } finally {
            assert lines != null;
            lines.close();
        }
        return listaUsuarios;
    }

}
