package ClasesPrograma.Ejercicios.EjerciciosHombre;

import InterfazPrograma.*;
import InterfazPrograma.informe.Informe;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;



public final class Ejer_ElevacionDiagonal extends javax.swing.JFrame {

   
    public Ejer_ElevacionDiagonal() {
        initComponents();
        this.setLocationRelativeTo(null);
        btInicio.setIcon(setIcono("/Multimedia/imagenes/inicio.png", btInicio));
        btninforme.setIcon(setIcono("/Multimedia/imagenes/informe.png", btninforme));
        btnestadist.setIcon(setIcono("/Multimedia/imagenes/estadistica.png", btnestadist));
        btnuser.setIcon(setIcono("/Multimedia/imagenes/user.png", btnuser));
        jlbejer.setIcon(SetjJLabel("/Multimedia/videos/VideosHombre/Elevación diagonal.gif", jlbejer));


    }

    public Icon setIcono(String url,JButton boton){
        ImageIcon icon = new ImageIcon(getClass().getResource(url));
        int ancho = boton.getWidth();
        int alto = boton.getHeight();
        ImageIcon icono = new ImageIcon(icon.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT));
        return icono;
    }
    public Icon SetjJLabel(String url,JLabel jlabel){
        ImageIcon icon = new ImageIcon(getClass().getResource(url));
        int ancho = jlabel.getWidth();
        int alto = jlabel.getHeight();
        ImageIcon icono = new ImageIcon(icon.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT));
        return icono;
    }
   
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jTextField2 = new javax.swing.JTextField();
        jMenuItem1 = new javax.swing.JMenuItem();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btInicio = new javax.swing.JButton();
        btninforme = new javax.swing.JButton();
        btnestadist = new javax.swing.JButton();
        btnuser = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jlbejer = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        Seleccionar = new javax.swing.JButton();

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(jList1);

        jTextField2.setText("jTextField2");

        jMenuItem1.setText("jMenuItem1");

        jLabel1.setText("jLabel1");

        jLabel2.setText("jLabel2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(390, 650));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btInicio.setBackground(new java.awt.Color(0, 51, 255));
        btInicio.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btInicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btInicioActionPerformed(evt);
            }
        });
        getContentPane().add(btInicio, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 600, 40, 40));

        btninforme.setBackground(new java.awt.Color(0, 51, 255));
        btninforme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btninformeActionPerformed(evt);
            }
        });
        getContentPane().add(btninforme, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 600, 40, 40));

        btnestadist.setBackground(new java.awt.Color(0, 51, 255));
        btnestadist.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnestadist.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnestadistActionPerformed(evt);
            }
        });
        getContentPane().add(btnestadist, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 600, 40, 40));

        btnuser.setBackground(new java.awt.Color(0, 51, 255));
        btnuser.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnuser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnuserActionPerformed(evt);
            }
        });
        getContentPane().add(btnuser, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 600, 40, 40));

        jTextField1.setBackground(new java.awt.Color(0, 51, 255));
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        getContentPane().add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 590, 390, 60));

        jlbejer.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jlbejer.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                jlbejerAncestorAdded(evt);
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        getContentPane().add(jlbejer, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 370, 210));

        jLabel3.setBackground(new java.awt.Color(51, 102, 255));
        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel3.setText("Ejercicio de Elevación diagonal");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 260, 290, 40));

        Seleccionar.setBackground(new java.awt.Color(102, 0, 204));
        Seleccionar.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        Seleccionar.setForeground(new java.awt.Color(255, 255, 255));
        Seleccionar.setText("Siguiente");
        Seleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SeleccionarActionPerformed(evt);
            }
        });
        getContentPane().add(Seleccionar, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 520, 130, 60));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void btInicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btInicioActionPerformed
      InicioApp form = new InicioApp();
       form.setVisible(true);
       this.dispose();
    }//GEN-LAST:event_btInicioActionPerformed

    private void btninformeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btninformeActionPerformed
        Informe form = new Informe();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btninformeActionPerformed

    private void btnestadistActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnestadistActionPerformed
        Estadistica form = new Estadistica();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnestadistActionPerformed

    private void btnuserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnuserActionPerformed
        User form = new User();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnuserActionPerformed

    private void jlbejerAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jlbejerAncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_jlbejerAncestorAdded

    private void SeleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SeleccionarActionPerformed
        Ejer_SentadillaPulgada form = new Ejer_SentadillaPulgada();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_SeleccionarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ejer_ElevacionDiagonal().setVisible(true);
            }
        });
    }
     

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Seleccionar;
    private javax.swing.JButton btInicio;
    private javax.swing.JButton btnestadist;
    private javax.swing.JButton btninforme;
    private javax.swing.JButton btnuser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JList<String> jList1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JLabel jlbejer;
    // End of variables declaration//GEN-END:variables
}
