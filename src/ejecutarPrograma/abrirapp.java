package ejecutarPrograma;

import InterfazPrograma.IniciarSesion;



public class abrirapp extends javax.swing.JFrame {

   
    public abrirapp() {
        initComponents();
        this.setLocationRelativeTo(null);
        
    }

    
    @SuppressWarnings("unchecked")
    
    
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        IngresarIniciarSesion = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(390, 600));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        IngresarIniciarSesion.setForeground(new java.awt.Color(255, 255, 255));
        IngresarIniciarSesion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Multimedia/imagenes/logo.png"))); // NOI18N
        IngresarIniciarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IngresarIniciarSesionActionPerformed(evt);
            }
        });
        getContentPane().add(IngresarIniciarSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(-2, 17, 390, 600));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void IngresarIniciarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IngresarIniciarSesionActionPerformed
        IniciarSesion form = new IniciarSesion();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_IngresarIniciarSesionActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new abrirapp().setVisible(true);
            }
        });
    }
     

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton IngresarIniciarSesion;
    // End of variables declaration//GEN-END:variables
}
