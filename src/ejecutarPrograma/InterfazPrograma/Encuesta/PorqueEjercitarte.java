package ejecutarPrograma.InterfazPrograma.Encuesta;

import InterfazPrograma.*;
import ClasesPrograma.TextPrompt;
import InterfazPrograma.IniciarSesion;
import ejecutarPrograma.InterfazPrograma.Encuesta.ProblemaSalud;


public class PorqueEjercitarte extends javax.swing.JFrame {

   
    public PorqueEjercitarte() {
        initComponents();
        this.setLocationRelativeTo(null);
        
        
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        Omtir = new javax.swing.JButton();
        Titulo = new javax.swing.JLabel();
        Seleccionar = new javax.swing.JButton();
        jRadioButton10 = new javax.swing.JRadioButton();
        jRadioButton11 = new javax.swing.JRadioButton();
        jRadioButton12 = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(390, 600));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Omtir.setBackground(new java.awt.Color(238, 238, 238));
        Omtir.setFont(new java.awt.Font("Arial Black", 1, 14)); // NOI18N
        Omtir.setText("Omitir");
        Omtir.setBorder(null);
        Omtir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OmtirActionPerformed(evt);
            }
        });
        getContentPane().add(Omtir, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 20, -1, -1));

        Titulo.setFont(new java.awt.Font("Perpetua", 1, 36)); // NOI18N
        Titulo.setText("<html>¿Porque deseas hacer ejercicio?<html>");
        getContentPane().add(Titulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 50, 340, 130));

        Seleccionar.setBackground(new java.awt.Color(102, 0, 204));
        Seleccionar.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        Seleccionar.setForeground(new java.awt.Color(255, 255, 255));
        Seleccionar.setText("Siguiente");
        Seleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SeleccionarActionPerformed(evt);
            }
        });
        getContentPane().add(Seleccionar, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 560, 120, 50));

        jRadioButton10.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jRadioButton10.setText("Por salud");
        jRadioButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton10ActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButton10, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 240, -1, -1));

        jRadioButton11.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jRadioButton11.setText("Mejor condicion fisica");
        jRadioButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton11ActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButton11, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 310, -1, -1));

        jRadioButton12.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jRadioButton12.setText("Razón personal");
        jRadioButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton12ActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButton12, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 370, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void OmtirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OmtirActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_OmtirActionPerformed

    private void SeleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SeleccionarActionPerformed
        IniciarSesion form = new IniciarSesion();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_SeleccionarActionPerformed

    private void jRadioButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton10ActionPerformed

    private void jRadioButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton11ActionPerformed

    private void jRadioButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton12ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PorqueEjercitarte().setVisible(true);
            }
        });
    }
     

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Omtir;
    private javax.swing.JButton Seleccionar;
    private javax.swing.JLabel Titulo;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JRadioButton jRadioButton10;
    private javax.swing.JRadioButton jRadioButton11;
    private javax.swing.JRadioButton jRadioButton12;
    // End of variables declaration//GEN-END:variables
}
