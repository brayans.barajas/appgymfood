package InterfazPrograma.Encuesta;

import ClasesPrograma.DatosUsuario;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;


public class genero extends javax.swing.JFrame {

   
    public genero() {
        initComponents();
        this.setLocationRelativeTo(null);
        jlbHombre.setIcon(SetjJLabel("/Multimedia/imagenes/Hombre.png", jlbHombre));
        jblMujer.setIcon(SetjJLabel("/Multimedia/imagenes/Mujer.png", jblMujer));

        
    }
    public Icon setIcono(String url,JButton boton){
        ImageIcon icon = new ImageIcon(getClass().getResource(url));
        int ancho = boton.getWidth();
        int alto = boton.getHeight();
        ImageIcon icono = new ImageIcon(icon.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT));
        return icono;
    }
    public Icon SetjJLabel(String url,JLabel jlabel){
        ImageIcon icon = new ImageIcon(getClass().getResource(url));
        int ancho = jlabel.getWidth();
        int alto = jlabel.getHeight();
        ImageIcon icono = new ImageIcon(icon.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT));
        return icono;
    }
    
    public void registrargenero() {
        DatosUsuario listDatoUsuario = new DatosUsuario();
               
        
        if (btrHombre.isSelected()) {
            listDatoUsuario.setGenero("Hombre");
            DatosUsuario.guardarArchivo(listDatoUsuario);
            Interes form = new Interes();
            form.setVisible(true);
            this.dispose();
        }else if (btrMujer.isSelected()) {
            listDatoUsuario.setGenero("Mujer");
            DatosUsuario.guardarArchivo(listDatoUsuario);
            Interes form = new Interes();
            form.setVisible(true);
            this.dispose();
        }else if (btrSinEspesificar.isSelected()) {
            listDatoUsuario.setGenero("Sin identificar");
            DatosUsuario.guardarArchivo(listDatoUsuario);
            Interes form = new Interes();
            form.setVisible(true);
            this.dispose();
        }else{
            JOptionPane.showMessageDialog(null, "Escoje un genero");

        }
        
        
        
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Titulo = new javax.swing.JLabel();
        Seleccionar = new javax.swing.JButton();
        btrHombre = new javax.swing.JRadioButton();
        jlbHombre = new javax.swing.JLabel();
        btrMujer = new javax.swing.JRadioButton();
        jblMujer = new javax.swing.JLabel();
        btrSinEspesificar = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(390, 650));
        setMinimumSize(new java.awt.Dimension(390, 650));
        setPreferredSize(new java.awt.Dimension(390, 650));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Titulo.setFont(new java.awt.Font("Perpetua", 1, 24)); // NOI18N
        Titulo.setText("Genero");
        getContentPane().add(Titulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 30, -1, -1));

        Seleccionar.setBackground(new java.awt.Color(102, 0, 204));
        Seleccionar.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        Seleccionar.setForeground(new java.awt.Color(255, 255, 255));
        Seleccionar.setText("Siguiente");
        Seleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SeleccionarActionPerformed(evt);
            }
        });
        getContentPane().add(Seleccionar, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 540, 160, 60));

        btrHombre.setText("Hombre");
        getContentPane().add(btrHombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 400, 100, 30));

        jlbHombre.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        getContentPane().add(jlbHombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 170, 310));

        btrMujer.setText("Mujer");
        getContentPane().add(btrMujer, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 400, 90, 30));

        jblMujer.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        getContentPane().add(jblMujer, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 90, 170, 310));

        btrSinEspesificar.setText("Prefiero no decir");
        getContentPane().add(btrSinEspesificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 440, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SeleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SeleccionarActionPerformed
        Interes form = new Interes();
            form.setVisible(true);
            this.dispose();
        
    }//GEN-LAST:event_SeleccionarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new genero().setVisible(true);
            }
        });
    }
     

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Seleccionar;
    private javax.swing.JLabel Titulo;
    private javax.swing.JRadioButton btrHombre;
    private javax.swing.JRadioButton btrMujer;
    private javax.swing.JRadioButton btrSinEspesificar;
    private javax.swing.JLabel jblMujer;
    private javax.swing.JLabel jlbHombre;
    // End of variables declaration//GEN-END:variables
}
