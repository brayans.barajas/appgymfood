package InterfazPrograma.Encuesta;

import InterfazPrograma.*;
import ClasesPrograma.TextPrompt;


public class Motivacion extends javax.swing.JFrame {

   
    public Motivacion() {
        initComponents();
        this.setLocationRelativeTo(null);
        
        
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Omtir = new javax.swing.JButton();
        Titulo = new javax.swing.JLabel();
        Seleccionar = new javax.swing.JButton();
        jRadioButton7 = new javax.swing.JRadioButton();
        jRadioButton8 = new javax.swing.JRadioButton();
        jRadioButton9 = new javax.swing.JRadioButton();
        jRadioButton10 = new javax.swing.JRadioButton();
        jRadioButton11 = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(390, 600));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Omtir.setBackground(new java.awt.Color(238, 238, 238));
        Omtir.setFont(new java.awt.Font("Arial Black", 1, 14)); // NOI18N
        Omtir.setText("Omitir");
        Omtir.setBorder(null);
        Omtir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OmtirActionPerformed(evt);
            }
        });
        getContentPane().add(Omtir, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 20, -1, -1));

        Titulo.setFont(new java.awt.Font("Perpetua", 1, 36)); // NOI18N
        Titulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Titulo.setText("<html>¿que te motiva a ejercitarte?<html>");
        Titulo.setToolTipText("");
        getContentPane().add(Titulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 20, 330, 100));

        Seleccionar.setBackground(new java.awt.Color(102, 0, 204));
        Seleccionar.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        Seleccionar.setForeground(new java.awt.Color(255, 255, 255));
        Seleccionar.setText("Siguiente");
        Seleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SeleccionarActionPerformed(evt);
            }
        });
        getContentPane().add(Seleccionar, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 560, 130, 60));

        jRadioButton7.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jRadioButton7.setText("Mejorar salud");
        jRadioButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton7ActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 130, -1, -1));

        jRadioButton8.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jRadioButton8.setText("Mejorar el sistema inmune");
        jRadioButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton8ActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButton8, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 200, -1, -1));

        jRadioButton9.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jRadioButton9.setText("Lucir mejor");
        jRadioButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton9ActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButton9, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 270, -1, -1));

        jRadioButton10.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jRadioButton10.setText("Aumentar fuerza cardiaca");
        jRadioButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton10ActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButton10, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 390, -1, -1));

        jRadioButton11.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jRadioButton11.setText("Desarollar fuerza fisca");
        jRadioButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton11ActionPerformed(evt);
            }
        });
        getContentPane().add(jRadioButton11, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 320, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void OmtirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OmtirActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_OmtirActionPerformed

    private void SeleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SeleccionarActionPerformed
       VecesEntrenar form = new VecesEntrenar();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_SeleccionarActionPerformed

    private void jRadioButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton7ActionPerformed

    private void jRadioButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton8ActionPerformed

    private void jRadioButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton9ActionPerformed

    private void jRadioButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton10ActionPerformed

    private void jRadioButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jRadioButton11ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Motivacion().setVisible(true);
            }
        });
    }
     

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Omtir;
    private javax.swing.JButton Seleccionar;
    private javax.swing.JLabel Titulo;
    private javax.swing.JRadioButton jRadioButton10;
    private javax.swing.JRadioButton jRadioButton11;
    private javax.swing.JRadioButton jRadioButton7;
    private javax.swing.JRadioButton jRadioButton8;
    private javax.swing.JRadioButton jRadioButton9;
    // End of variables declaration//GEN-END:variables
}
