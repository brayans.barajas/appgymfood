package InterfazPrograma.Encuesta;

import ClasesPrograma.DatosUsuario;
import static ClasesPrograma.DatosUsuario.leerArchivoUsuario;
import ClasesPrograma.TextPrompt;
import InterfazPrograma.InicioApp;

import javax.swing.JOptionPane;
import java.util.ArrayList;

public class pesoAltura extends javax.swing.JFrame {

    public pesoAltura() {
        initComponents();
        TextPrompt usuario = new TextPrompt("Ingrese su peso", usuarioLogin);
        TextPrompt contraseña = new TextPrompt("Ingrese su altura", contraseñaLogin);
        this.setLocationRelativeTo(null);
        this.setTitle("ingresar peso y altura");

    }

    @SuppressWarnings("unchecked")

   


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        usuarioLogin = new javax.swing.JTextField();
        contraseñaLogin = new javax.swing.JPasswordField();
        Ingresar1 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(390, 650));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        usuarioLogin.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        usuarioLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usuarioLoginActionPerformed(evt);
            }
        });
        getContentPane().add(usuarioLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 240, 230, 60));

        contraseñaLogin.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        contraseñaLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                contraseñaLoginActionPerformed(evt);
            }
        });
        getContentPane().add(contraseñaLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 320, 240, 50));

        Ingresar1.setBackground(new java.awt.Color(102, 0, 204));
        Ingresar1.setFont(new java.awt.Font("Arial Black", 1, 18)); // NOI18N
        Ingresar1.setText("Siguiente");
        Ingresar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Ingresar1ActionPerformed(evt);
            }
        });
        getContentPane().add(Ingresar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 520, 180, 70));

        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel3.setText("Cuéntanos más sobre ti");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 20, -1, -1));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel1.setText("<html>Dejanos conocerte mejor  para potenciar tus resultados<html>");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 80, 310, 80));

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void usuarioLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usuarioLoginActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_usuarioLoginActionPerformed

    private void contraseñaLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contraseñaLoginActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_contraseñaLoginActionPerformed

    private void Ingresar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Ingresar1ActionPerformed
        Interes form = new Interes();
                form.setVisible(true);
                this.dispose();
    }//GEN-LAST:event_Ingresar1ActionPerformed


    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new pesoAltura().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Ingresar1;
    private javax.swing.JPasswordField contraseñaLogin;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField usuarioLogin;
    // End of variables declaration//GEN-END:variables
}
