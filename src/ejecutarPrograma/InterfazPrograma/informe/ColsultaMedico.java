package ejecutarPrograma.InterfazPrograma.informe;

import InterfazPrograma.Estadistica;
import InterfazPrograma.InicioApp;
import InterfazPrograma.User;
import InterfazPrograma.informe.Informe;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;



public class ColsultaMedico extends javax.swing.JFrame {

   
    public ColsultaMedico() {
        initComponents();
        this.setLocationRelativeTo(null);
        btInicio.setIcon(setIcono("/Multimedia/imagenes/inicio.png", btInicio));
        btninforme.setIcon(setIcono("/Multimedia/imagenes/informe.png", btninforme));
        btnestadist.setIcon(setIcono("/Multimedia/imagenes/estadistica.png", btnestadist));
        btnuser.setIcon(setIcono("/Multimedia/imagenes/user.png", btnuser));

    }

    public Icon setIcono(String url,JButton boton){
        ImageIcon icon = new ImageIcon(getClass().getResource(url));
        int ancho = boton.getWidth();
        int alto = boton.getHeight();
        ImageIcon icono = new ImageIcon(icon.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT));
        return icono;
    }
    public Icon SetjJLabel(String url,JLabel jlabel){
        ImageIcon icon = new ImageIcon(getClass().getResource(url));
        int ancho = jlabel.getWidth();
        int alto = jlabel.getHeight();
        ImageIcon icono = new ImageIcon(icon.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT));
        return icono;
    }
   
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jTextField2 = new javax.swing.JTextField();
        jMenuItem1 = new javax.swing.JMenuItem();
        btInicio = new javax.swing.JButton();
        btninforme = new javax.swing.JButton();
        btnestadist = new javax.swing.JButton();
        btnuser = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(jList1);

        jTextField2.setText("jTextField2");

        jMenuItem1.setText("jMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(390, 650));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btInicio.setBackground(new java.awt.Color(0, 51, 255));
        btInicio.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btInicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btInicioActionPerformed(evt);
            }
        });
        getContentPane().add(btInicio, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 600, 40, 40));

        btninforme.setBackground(new java.awt.Color(0, 51, 255));
        btninforme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btninformeActionPerformed(evt);
            }
        });
        getContentPane().add(btninforme, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 600, 40, 40));

        btnestadist.setBackground(new java.awt.Color(0, 51, 255));
        btnestadist.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnestadist.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnestadistActionPerformed(evt);
            }
        });
        getContentPane().add(btnestadist, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 600, 40, 40));

        btnuser.setBackground(new java.awt.Color(0, 51, 255));
        btnuser.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnuser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnuserActionPerformed(evt);
            }
        });
        getContentPane().add(btnuser, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 600, 40, 40));

        jTextField1.setBackground(new java.awt.Color(0, 51, 255));
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        getContentPane().add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 590, 390, 60));

        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Pristina", 0, 24)); // NOI18N
        jTextArea1.setRows(5);
        jTextArea1.setText("Razones para ir al médico");
        jScrollPane2.setViewportView(jTextArea1);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 260, 40));

        jTextArea2.setColumns(20);
        jTextArea2.setFont(new java.awt.Font("Eras Medium ITC", 0, 12)); // NOI18N
        jTextArea2.setRows(5);
        jTextArea2.setText("Aunque la actividad física moderada, como\ncaminar a paso ligero, sea segura para la \nmayoría de las personas, los expertos en salud \nsugieren que hables con tu médico antes de\ncomenzar un programa de ejercicio en\ncualquiera de las siguientes situaciones:\n\n*Si tienes enfermedad cardíaca.\n*Si tienes diabetes de tipo 1 o tipo 2.\n*Si tienes una enfermedad renal.\n*Si tienes artritis.\n*Si estás recibiendo tratamiento para el cáncer\no si has completado uno recientemente.\n*Si tienes la presión arterial alta.\n\nEn general, si no has hecho ejercicio\nregularmente por un tiempo, puedes comenzar \na hacer ejercicio a un nivel de leve a moderado\nsin consultar al médico e incrementar\ngradualmente tu actividad.\n\nTambién puedes consultar con tu médico si \ntienes síntomas que puedan estar relacionados\ncon el corazón, los pulmones u otras\nenfermedades graves, tales como:\n\nDolor o molestia en el pecho, el cuello, la\nmandíbula o los brazos en reposo o durante la\nactividad física Mareos, aturdimiento o desmayos al\nhacer ejercicio o esfuerzo. \n\nDificultad para respirar con un esfuerzo leve, en\nreposo o al acostarte o ir a dormir Inflamación en\nlos tobillos, especialmente por la noche\nUn latido cardíaco rápido o pronunciado\nUn soplo cardíaco que el médico te haya\ndiagnosticado previamente\n\nDolor en la parte baja de la pierna al caminar \nque desaparece con el reposo\n\nSi tienes enfermedad cardíaca, enfermedad renal\no diabetes de tipo 1 o tipo 2, pero no tienes \nsíntomas y normalmente no haces ejercicio\nTienes cualquier síntoma de enfermedad\ncardíaca, enfermedad renal o diabetes de \ntipo 1 o tipo 2\n\nSi tienes dudas, analízalo\nSi tienes dudas sobre tu estado de salud, tienes\n varios problemas de salud o estás embarazada, \nhabla con el médico antes de comenzar un programa\n de ejercicios nuevo. Trabajar con el médico con \nantelación puede ayudarte a planear el programa de\nejercicios adecuado para ti. Ese es un buen primer\npaso en tu camino hacia un buen estado físico.");
        jScrollPane3.setViewportView(jTextArea2);

        getContentPane().add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 360, 500));

        jButton1.setText("regresar");
        jButton1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 70, 30));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void btInicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btInicioActionPerformed
      InicioApp form = new InicioApp();
       form.setVisible(true);
       this.dispose();
    }//GEN-LAST:event_btInicioActionPerformed

    private void btninformeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btninformeActionPerformed
        Informe form = new Informe();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btninformeActionPerformed

    private void btnestadistActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnestadistActionPerformed
        Estadistica form = new Estadistica();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnestadistActionPerformed

    private void btnuserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnuserActionPerformed
        User form = new User();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnuserActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        informeSalud form = new informeSalud();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ColsultaMedico().setVisible(true);
            }
        });
    }
     

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btInicio;
    private javax.swing.JButton btnestadist;
    private javax.swing.JButton btninforme;
    private javax.swing.JButton btnuser;
    private javax.swing.JButton jButton1;
    private javax.swing.JList<String> jList1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}
