package InterfazPrograma;

import static ClasesPrograma.DatosUsuario.leerArchivoUsuario;
import ClasesPrograma.TextPrompt;
import javax.swing.JOptionPane;

public class IniciarSesion extends javax.swing.JFrame {

    public IniciarSesion() {
        initComponents();
        TextPrompt usuario = new TextPrompt("Ingrese Usuario", usuarioLogin);
        TextPrompt contraseña = new TextPrompt("Ingrese la contraseña", contraseñaLogin);
        this.setLocationRelativeTo(null);
        this.setTitle("Inicio de sesion");

    }

    @SuppressWarnings("unchecked")

    public void iniciarSistema() {

        String usuario = usuarioLogin.getText();
        String contraseña = String.valueOf(contraseñaLogin.getPassword());
        boolean estado = false;

        for (int i = 0; i < leerArchivoUsuario().size(); i++) {

            if ((leerArchivoUsuario().get(i).getUsuario().equals(usuario)) && leerArchivoUsuario().get(i).getContraseña().equals(contraseña)) {
                JOptionPane.showMessageDialog(null, "bienvenido usuario " + usuario);
                estado = true;
                InicioApp form = new InicioApp();
                form.setVisible(true);
                this.dispose();
                break;

            }

        }
        if (estado == false) {
            JOptionPane.showMessageDialog(null, "Error al iniciar sesion  contraseña incorrecta o usuario");
            usuarioLogin.setText("");
            contraseñaLogin.setText("");
        }

    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        usuarioLogin = new javax.swing.JTextField();
        contraseñaLogin = new javax.swing.JPasswordField();
        Ingresar1 = new javax.swing.JButton();
        recuperarcontraseña = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        InicioDeSesion1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(390, 650));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Multimedia/imagenes/logo2.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 360, 230));

        usuarioLogin.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        usuarioLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usuarioLoginActionPerformed(evt);
            }
        });
        getContentPane().add(usuarioLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 280, 260, 40));

        contraseñaLogin.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        contraseñaLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                contraseñaLoginActionPerformed(evt);
            }
        });
        getContentPane().add(contraseñaLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 340, 260, 40));

        Ingresar1.setBackground(new java.awt.Color(102, 0, 204));
        Ingresar1.setFont(new java.awt.Font("Arial Black", 1, 14)); // NOI18N
        Ingresar1.setText("INICIAR SESION");
        Ingresar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Ingresar1ActionPerformed(evt);
            }
        });
        getContentPane().add(Ingresar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 520, 170, 40));

        recuperarcontraseña.setBackground(new java.awt.Color(242, 242, 242));
        recuperarcontraseña.setFont(new java.awt.Font("Arial Black", 0, 10)); // NOI18N
        recuperarcontraseña.setForeground(new java.awt.Color(51, 51, 255));
        recuperarcontraseña.setText("¿Has olvidado tu contraseña?");
        recuperarcontraseña.setBorderPainted(false);
        recuperarcontraseña.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recuperarcontraseñaActionPerformed(evt);
            }
        });
        getContentPane().add(recuperarcontraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 390, -1, -1));

        jLabel2.setBackground(new java.awt.Color(102, 0, 204));
        jLabel2.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Inice sesión");
        jLabel2.setToolTipText("");
        jLabel2.setOpaque(true);
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 220, 120, 40));

        InicioDeSesion1.setBackground(new java.awt.Color(242, 242, 242));
        InicioDeSesion1.setFont(new java.awt.Font("Arial Black", 0, 12)); // NOI18N
        InicioDeSesion1.setText("¿No tienes cuenta?Crear una cuenta");
        InicioDeSesion1.setBorderPainted(false);
        InicioDeSesion1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InicioDeSesion1ActionPerformed(evt);
            }
        });
        getContentPane().add(InicioDeSesion1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 580, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void usuarioLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usuarioLoginActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_usuarioLoginActionPerformed

    private void contraseñaLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contraseñaLoginActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_contraseñaLoginActionPerformed

    private void Ingresar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Ingresar1ActionPerformed
        iniciarSistema();
    }//GEN-LAST:event_Ingresar1ActionPerformed


    private void recuperarcontraseñaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_recuperarcontraseñaActionPerformed
        JOptionPane.showMessageDialog(null, """
                                           para recuperar tu contraseña o restablecerla 
                                            ingrese la repuesta de la suma 
                                           """);
        RecuperarContraseña form = new RecuperarContraseña();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_recuperarcontraseñaActionPerformed

    private void InicioDeSesion1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InicioDeSesion1ActionPerformed
        Registro form = new Registro();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_InicioDeSesion1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new IniciarSesion().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Ingresar1;
    private javax.swing.JButton InicioDeSesion1;
    private javax.swing.JPasswordField contraseñaLogin;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JButton recuperarcontraseña;
    private javax.swing.JTextField usuarioLogin;
    // End of variables declaration//GEN-END:variables
}
