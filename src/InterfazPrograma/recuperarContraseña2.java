package InterfazPrograma;

import static ClasesPrograma.DatosUsuario.leerArchivoUsuario;
import InterfazPrograma.IniciarSesion;
import ClasesPrograma.TextPrompt;
import InterfazPrograma.InicioApp;
import java.io.FileWriter;
import javax.swing.JOptionPane;


public class recuperarContraseña2 extends javax.swing.JFrame {

   
    public recuperarContraseña2() {
        initComponents();
        TextPrompt Usuario = new TextPrompt("Escriba  tu usuario",this.Usuario);
        TextPrompt nuevaCOntraseña = new TextPrompt("Escriba  tu contraseña",this.Nuevacontraseña);
        TextPrompt verificarNuevaContraseña = new TextPrompt("Escriba  tu contraseña",this.verifivarNuevacontraseña);

        this.setLocationRelativeTo(null);
        this.setTitle("Recuperar contraseña");
        
    }
    
    public void CambioContrasela() {     

        String verusuario = Usuario.getText();
        String Contraseña = String.valueOf(Nuevacontraseña.getPassword());
        String ConfirmarContraseña = String.valueOf( verifivarNuevacontraseña.getPassword());
        boolean estado = false;
        for (int i = 0; i < leerArchivoUsuario().size(); i++) {
            
            if ((leerArchivoUsuario().get(i).getUsuario().equals(verusuario))) {
                
                
                leerArchivoUsuario().get(i).setContraseña(Contraseña);
                estado= true;
                                
                IniciarSesion form = new IniciarSesion();
                form.setVisible(true);
                this.dispose();
                break;
                
            }
            
        }
        if(estado==false){
                JOptionPane.showMessageDialog(null, "Error usuario no existe");
                         
            }
       

    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        Usuario = new javax.swing.JTextField();
        Verificar = new javax.swing.JToggleButton();
        jLabel3 = new javax.swing.JLabel();
        verifivarNuevacontraseña = new javax.swing.JPasswordField();
        Nuevacontraseña = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(390, 600));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Multimedia/imagenes/logo2.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 360, 230));

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel2.setText("Escribe tu usuario");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 250, -1, -1));

        Usuario.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        getContentPane().add(Usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 290, 200, 40));

        Verificar.setBackground(new java.awt.Color(0, 102, 255));
        Verificar.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        Verificar.setText("Verificar");
        Verificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VerificarActionPerformed(evt);
            }
        });
        getContentPane().add(Verificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 540, 140, 50));

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel3.setText("Ahora escribe tu nueva contraseña");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 360, -1, -1));
        getContentPane().add(verifivarNuevacontraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 470, 260, 50));
        getContentPane().add(Nuevacontraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 400, 260, 50));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void VerificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VerificarActionPerformed
        CambioContrasela();
    }//GEN-LAST:event_VerificarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new recuperarContraseña2().setVisible(true);
            }
        });
    }
     

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField Nuevacontraseña;
    private javax.swing.JTextField Usuario;
    private javax.swing.JToggleButton Verificar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPasswordField verifivarNuevacontraseña;
    // End of variables declaration//GEN-END:variables
}
