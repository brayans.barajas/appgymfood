package InterfazPrograma;

import ClasesPrograma.Ejercicios.EjerciciosHombre.EJer_Resistencia;
import ClasesPrograma.Ejercicios.EjerciciosMujer.SentadillasM;
import InterfazPrograma.informe.Informe;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;



public class InicioApp extends javax.swing.JFrame {

   
    public InicioApp() {
        initComponents();
        this.setLocationRelativeTo(null);
         btInicio.setIcon(setIcono("/Multimedia/imagenes/inicio.png", btInicio));
        btninforme.setIcon(setIcono("/Multimedia/imagenes/informe.png", btninforme));
        btnestadist.setIcon(setIcono("/Multimedia/imagenes/estadistica.png", btnestadist));
        btnuser.setIcon(setIcono("/Multimedia/imagenes/user.png", btnuser));
        jlhombre.setIcon(SetjJLabel("/Multimedia/videos/VideosHombre/Arnorlpres.gif", jlhombre));
        jlMUjer.setIcon(SetjJLabel("/Multimedia/videos/VideosMujer/SentadillasM.gif", jlMUjer));   

    }

    public Icon setIcono(String url,JButton boton){
        ImageIcon icon = new ImageIcon(getClass().getResource(url));
        int ancho = boton.getWidth();
        int alto = boton.getHeight();
        ImageIcon icono = new ImageIcon(icon.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT));
        return icono;
    }
     public Icon SetjJLabel(String url,JLabel jlabel){
        ImageIcon icon = new ImageIcon(getClass().getResource(url));
        int ancho = jlabel.getWidth();
        int alto = jlabel.getHeight();
        ImageIcon icono = new ImageIcon(icon.getImage().getScaledInstance(ancho, alto, Image.SCALE_DEFAULT));
        return icono;
    }
   
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jTextField2 = new javax.swing.JTextField();
        jMenuItem1 = new javax.swing.JMenuItem();
        btInicio = new javax.swing.JButton();
        btninforme = new javax.swing.JButton();
        btnestadist = new javax.swing.JButton();
        btnuser = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jlMUjer = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jlhombre = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(jList1);

        jTextField2.setText("jTextField2");

        jMenuItem1.setText("jMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setMinimumSize(new java.awt.Dimension(390, 650));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btInicio.setBackground(new java.awt.Color(0, 51, 255));
        btInicio.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btInicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btInicioActionPerformed(evt);
            }
        });
        getContentPane().add(btInicio, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 600, 40, 40));

        btninforme.setBackground(new java.awt.Color(0, 51, 255));
        btninforme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btninformeActionPerformed(evt);
            }
        });
        getContentPane().add(btninforme, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 600, 40, 40));

        btnestadist.setBackground(new java.awt.Color(0, 51, 255));
        btnestadist.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnestadist.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnestadistActionPerformed(evt);
            }
        });
        getContentPane().add(btnestadist, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 600, 40, 40));

        btnuser.setBackground(new java.awt.Color(0, 51, 255));
        btnuser.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnuser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnuserActionPerformed(evt);
            }
        });
        getContentPane().add(btnuser, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 600, 40, 40));

        jTextField1.setBackground(new java.awt.Color(0, 51, 255));
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        getContentPane().add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 590, 390, 60));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Multimedia/imagenes/superior.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 390, 90));

        jlMUjer.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jlMUjer.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                jlMUjerAncestorAdded(evt);
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        jlMUjer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlMUjerMouseClicked(evt);
            }
        });
        getContentPane().add(jlMUjer, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 390, 310, 150));

        jLabel2.setFont(new java.awt.Font("Segoe UI", 3, 18)); // NOI18N
        jLabel2.setText("Ejercicios Mujer");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 340, -1, -1));

        jlhombre.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jlhombre.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                jlhombreAncestorAdded(evt);
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        jlhombre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlhombreMouseClicked(evt);
            }
        });
        getContentPane().add(jlhombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, 310, 150));

        jLabel3.setFont(new java.awt.Font("Segoe UI", 3, 18)); // NOI18N
        jLabel3.setText("Ejercicios hombre");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 130, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void btInicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btInicioActionPerformed
       InicioApp form = new InicioApp();
       form.setVisible(true);
       this.dispose();
    }//GEN-LAST:event_btInicioActionPerformed

    private void btninformeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btninformeActionPerformed
        Informe form = new Informe();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btninformeActionPerformed

    private void btnestadistActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnestadistActionPerformed
        Estadistica form = new Estadistica();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnestadistActionPerformed

    private void btnuserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnuserActionPerformed
        User form = new User();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnuserActionPerformed

    private void jlMUjerAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jlMUjerAncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_jlMUjerAncestorAdded

    private void jlMUjerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlMUjerMouseClicked
        SentadillasM form = new SentadillasM();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlMUjerMouseClicked

    private void jlhombreAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jlhombreAncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_jlhombreAncestorAdded

    private void jlhombreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlhombreMouseClicked
        EJer_Resistencia form = new EJer_Resistencia();
        form.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jlhombreMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InicioApp().setVisible(true);
            }
        });
    }
     

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btInicio;
    private javax.swing.JButton btnestadist;
    private javax.swing.JButton btninforme;
    private javax.swing.JButton btnuser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JList<String> jList1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JLabel jlMUjer;
    private javax.swing.JLabel jlhombre;
    // End of variables declaration//GEN-END:variables
}
