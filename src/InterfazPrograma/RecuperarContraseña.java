package InterfazPrograma;

import ClasesPrograma.TextPrompt;
import static java.lang.Integer.parseInt;
import javax.swing.JOptionPane;


public class RecuperarContraseña extends javax.swing.JFrame {

   
    public RecuperarContraseña() {
        initComponents();
        jlbproblema.setText("¿Cuanto es " + numero1 +" + " +numero2);
        TextPrompt respuesta = new TextPrompt("tu respuesta",this.respuesta);
        this.setLocationRelativeTo(null);
        this.setTitle("Recuperar contraseña");
        
    }
    int numero1 = (int) (Math.random() * 50 + 1);
    int numero2 = (int) (Math.random() * 50 + 1);
     
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        respuesta = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jlbproblema = new javax.swing.JLabel();
        Verificar = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMinimumSize(new java.awt.Dimension(390, 600));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        respuesta.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        getContentPane().add(respuesta, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 390, 250, 40));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Multimedia/imagenes/logo2.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 360, 230));

        jlbproblema.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jlbproblema.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(jlbproblema, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 230, 290, 50));

        Verificar.setBackground(new java.awt.Color(0, 102, 255));
        Verificar.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        Verificar.setText("Verificar");
        Verificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VerificarActionPerformed(evt);
            }
        });
        getContentPane().add(Verificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 440, 140, 50));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void VerificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VerificarActionPerformed
        int problema = numero1+numero2; 
        String verRespuesta = this.respuesta.getText();
        if(parseInt(verRespuesta) == problema){
            recuperarContraseña2 form = new recuperarContraseña2();
            form.setVisible(true);
            this.dispose();
        }else{
            JOptionPane.showMessageDialog(null, "valor incorrecto");
            RecuperarContraseña form = new RecuperarContraseña();
            form.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_VerificarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RecuperarContraseña().setVisible(true);
            }
        });
    }
     

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton Verificar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jlbproblema;
    private javax.swing.JTextField respuesta;
    // End of variables declaration//GEN-END:variables
}
