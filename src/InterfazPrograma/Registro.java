package InterfazPrograma;

import ClasesPrograma.TextPrompt;
import ClasesPrograma.DatosUsuario;
import InterfazPrograma.Encuesta.genero;
import javax.swing.JOptionPane;

public class Registro extends javax.swing.JFrame {

    public Registro() {
        initComponents();
        this.setTitle("Registro de usuario");
        this.setLocationRelativeTo(null);
        TextPrompt Nombre = new TextPrompt("Nombre", NombreNuevo);
        TextPrompt IngresarEmail = new TextPrompt("Ingrese Usuario", UsuarioNuevo);
        TextPrompt IngreseContraseña = new TextPrompt("Ingrese su contraseña", contraseñaNueva);
        TextPrompt ConfirmeContraseña = new TextPrompt("Confirme su contraseña", ConfirmaPasswordNueva);
    }

    @SuppressWarnings("unchecked")

    public void registroUsuario() {
        DatosUsuario listDatoUsuario = new DatosUsuario();

        String Nombre = NombreNuevo.getText();
        String Usuario = UsuarioNuevo.getText();
        String Contraseña = String.valueOf(contraseñaNueva.getPassword());
        String ConfirmarContraseña = String.valueOf(ConfirmaPasswordNueva.getPassword());
        String dia =(String) Diauser.getSelectedItem();
        String mes =(String) MesUser.getSelectedItem();
        String año =(String) AñoUser.getSelectedItem();

        listDatoUsuario.setNombre(Nombre);
        listDatoUsuario.setUsuario(Usuario);
        listDatoUsuario.setContraseña(Contraseña);
        listDatoUsuario.setConfirmarContraseña(ConfirmarContraseña);
        listDatoUsuario.setDia(dia);
        listDatoUsuario.setMes(mes);
        listDatoUsuario.setAño(año);

        
            
        if (Contraseña.equals(ConfirmarContraseña)) {
            DatosUsuario.guardarArchivo(listDatoUsuario);
            JOptionPane.showMessageDialog(null, "Registro existoso, continua con la encuesta");
            genero form = new genero();
            form.setVisible(true);
            this.dispose();
        } else {
            JOptionPane.showMessageDialog(null, "Contraseñas diferentes");
            contraseñaNueva.setText(null);
            ConfirmaPasswordNueva.setText(null);

        }
                
            
        

    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Logo2 = new javax.swing.JLabel();
        Año = new javax.swing.JLabel();
        UsuarioNuevo = new javax.swing.JTextField();
        ConfirmaPasswordNueva = new javax.swing.JPasswordField();
        Registrarse = new javax.swing.JButton();
        InicioDeSesion = new javax.swing.JButton();
        NombreNuevo = new javax.swing.JTextField();
        contraseñaNueva = new javax.swing.JPasswordField();
        Diauser = new javax.swing.JComboBox();
        EscogerFechaNacimiento = new javax.swing.JLabel();
        Dia = new javax.swing.JLabel();
        MesUser = new javax.swing.JComboBox();
        Mes = new javax.swing.JLabel();
        AñoUser = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setExtendedState(1);
        setMinimumSize(new java.awt.Dimension(390, 650));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Logo2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Multimedia/imagenes/logo2.png"))); // NOI18N
        getContentPane().add(Logo2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 360, 230));

        Año.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        Año.setText("Año");
        getContentPane().add(Año, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 510, 40, 20));

        UsuarioNuevo.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        UsuarioNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UsuarioNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(UsuarioNuevo, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 300, 260, 40));

        ConfirmaPasswordNueva.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        ConfirmaPasswordNueva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConfirmaPasswordNuevaActionPerformed(evt);
            }
        });
        getContentPane().add(ConfirmaPasswordNueva, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 420, 260, 40));

        Registrarse.setBackground(new java.awt.Color(102, 0, 204));
        Registrarse.setFont(new java.awt.Font("Arial Black", 1, 14)); // NOI18N
        Registrarse.setForeground(new java.awt.Color(255, 255, 255));
        Registrarse.setText("Registrarse");
        Registrarse.setBorder(null);
        Registrarse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RegistrarseActionPerformed(evt);
            }
        });
        getContentPane().add(Registrarse, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 550, 140, 40));

        InicioDeSesion.setBackground(new java.awt.Color(242, 242, 242));
        InicioDeSesion.setFont(new java.awt.Font("Arial Black", 1, 14)); // NOI18N
        InicioDeSesion.setText("Iniciar sesión");
        InicioDeSesion.setBorderPainted(false);
        InicioDeSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InicioDeSesionActionPerformed(evt);
            }
        });
        getContentPane().add(InicioDeSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 590, -1, -1));

        NombreNuevo.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        NombreNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NombreNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(NombreNuevo, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 240, 260, 40));

        contraseñaNueva.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        contraseñaNueva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                contraseñaNuevaActionPerformed(evt);
            }
        });
        getContentPane().add(contraseñaNueva, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 360, 260, 40));

        Diauser.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));
        Diauser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DiauserActionPerformed(evt);
            }
        });
        getContentPane().add(Diauser, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 510, 40, -1));

        EscogerFechaNacimiento.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        EscogerFechaNacimiento.setText("Escoja su fecha de nacimiento");
        getContentPane().add(EscogerFechaNacimiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 470, 300, 30));

        Dia.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        Dia.setText("Dia");
        getContentPane().add(Dia, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 510, 30, 20));

        MesUser.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));
        getContentPane().add(MesUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 510, 90, -1));

        Mes.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        Mes.setText("Mes");
        getContentPane().add(Mes, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 510, 40, 20));

        AñoUser.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "2022", "2021", "2020", "2019", "2018", "2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995", "1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987", "1986", "1985", "1984", "1983", "1982", "1981", "1980", "1979", "1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971", "1970", "1969", "1968", "1967", "1966", "1965", "1964", "1963", "1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955", "1954", "1953", "1952", "1951", "1950", "1949", "1948", "1947", "1946", "1945", "1944", "1943", "1942", "1941", "1940", "1939", "1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931", "1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923", "1922" }));
        AñoUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AñoUserActionPerformed(evt);
            }
        });
        getContentPane().add(AñoUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 510, -1, -1));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents


    private void UsuarioNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UsuarioNuevoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_UsuarioNuevoActionPerformed

    private void ConfirmaPasswordNuevaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConfirmaPasswordNuevaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ConfirmaPasswordNuevaActionPerformed

    private void RegistrarseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RegistrarseActionPerformed
        registroUsuario();


    }//GEN-LAST:event_RegistrarseActionPerformed

    private void InicioDeSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InicioDeSesionActionPerformed
        IniciarSesion form = new IniciarSesion();
        form.setVisible(true);
        this.dispose();

    }//GEN-LAST:event_InicioDeSesionActionPerformed

    private void NombreNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NombreNuevoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NombreNuevoActionPerformed

    private void contraseñaNuevaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contraseñaNuevaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_contraseñaNuevaActionPerformed

    private void DiauserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DiauserActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DiauserActionPerformed

    private void AñoUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AñoUserActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_AñoUserActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Registro().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Año;
    private javax.swing.JComboBox AñoUser;
    private javax.swing.JPasswordField ConfirmaPasswordNueva;
    private javax.swing.JLabel Dia;
    private javax.swing.JComboBox Diauser;
    private javax.swing.JLabel EscogerFechaNacimiento;
    private javax.swing.JButton InicioDeSesion;
    private javax.swing.JLabel Logo2;
    private javax.swing.JLabel Mes;
    private javax.swing.JComboBox MesUser;
    private javax.swing.JTextField NombreNuevo;
    private javax.swing.JButton Registrarse;
    private javax.swing.JTextField UsuarioNuevo;
    private javax.swing.JPasswordField contraseñaNueva;
    // End of variables declaration//GEN-END:variables
}
